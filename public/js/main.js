window.addEventListener('scroll', (evt) => {
    let navbar = document.querySelector('#navbar')
    let nav = document.querySelector('.navbar')
    let logo = document.querySelector('#logo')

    if (navbar.offsetTop > 0) {
        navbar.classList.add('shadow')
        logo.height = '50'
    } else {
        navbar.classList.remove('shadow')
        logo.height = '70'
    }
});

$('#imagePreviev').on('show.bs.modal', (event) => {
    var button = $(event.relatedTarget) // Button that triggered the modal
    var recipient = button.data('whatever') // Extract info from data-* attributes

    var modal = document.querySelector('#modal-img')
    modal.src = recipient
})